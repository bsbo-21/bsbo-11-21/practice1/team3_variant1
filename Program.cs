using System.Reflection.Metadata;

namespace ATM
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<(int, long, string)> list = new List<(int, long, string)>();
            list.Add((123, 89291044893, "Abboba Abobus Abobovich"));
            Bank bank = new Bank(list);

            Money money = new Money(1000);
            Card card = new Card(123, 89291044893, "Abboba Abobus Abobovich", 5000);
            ATM atm = new ATM(bank, money);
            atm.GetCard(card);
            atm.CheckMoney();
            atm.GetMoney(500);
            atm.GiveMoney(400);
            atm.CheckMoney();
            atm.GiveCard();
        }
    }

    public class Bank
    {
        private List<(int, long, string)> users;
        public List<(int, long, string)> Users()
        {
            Console.WriteLine("Bank.Users\n");
            return users;
        }
        public bool Conf(Card card)
        {
            Console.WriteLine("Bank.Conf\n");
            for (int i = 0; i < users.Count(); i++)
            {
                if (users[i].Item1 == card.CVV &&
                    users[i].Item2 == card._number &&
                    users[i].Item3 == card._ownerName)
                {
                    return true;
                }
            }
            return false;
        }
        public Bank(List<(int, long, string)> users)
        {
            Console.WriteLine("Create Class Bank\n");
            this.users = users;
        }
    }

    public class Money
    {
        public int denomination { get; set; }
        public Money(int denomination)
        {
            Console.WriteLine("Create Class Money\n");
            this.denomination = denomination;
        }
    }

    public class Card
    {
        public int CVV;
        public long _number { get; set; }
        public string _ownerName { get; set; }
        private int balance;
        public int GetBalance()
        {
            Console.WriteLine("Card.GetBalance\n");
            return balance;
        }
        public int SetBalance(int value) 
        {
            Console.WriteLine("Card.SetBalance\n");
            this.balance = value;
            return this.balance;
        }

        public Card(int CVV, long _number, string _ownerName, int balance)
        {
            Console.WriteLine("Create Class Card\n");
            this.CVV = CVV;
            this._number = _number;
            this._ownerName = _ownerName;
            this.balance = balance;
        }
    }

    public class ATM
    {
        private Card card { get; set; }
        private Bank bank { get; set; }
        private Money money { get; set; }

        public void GetCard(Card card)
        {
            Console.WriteLine("ATM.GetCard\n");
            Console.WriteLine("-----------Card inserted-----------\n");
            this.card = card;
        }
        public void GiveCard()
        {
            Console.WriteLine("ATM.GiveCard\n");
            Console.WriteLine("-----------Card issued-----------\n");
            this.card = null;
        }

        public bool Auth()
        {
            Console.WriteLine("ATM.Auth\n");
            return bank.Conf(card);
        }

        public void GiveMoney(int amount)
        {
            Console.WriteLine("ATM.GiveMoney\n");
            if (Auth() && card.GetBalance() > amount)
            {
                money.denomination -= amount;
                card.SetBalance(card.GetBalance() - amount);
                Console.WriteLine($"-----------Issued {amount} money-----------\n");
            }
            else
            {
                Console.WriteLine("-----------Something went wrong-----------\n");
            }
        }

        public void GetMoney(int amount)
        {
            Console.WriteLine("ATM.GetMoney\n");
            if (Auth())
            {
                money.denomination += amount;
                card.SetBalance(card.GetBalance() + amount);
                Console.WriteLine($"-----------Contributed {amount} money-----------\n");
            }
            else
            {
                Console.WriteLine("-----------Something went wrong-----------");
            }
        }

        public void CheckMoney()
        {
            Console.WriteLine("ATM.CheckMoney\n");
            Console.WriteLine($"-----------On account {card.GetBalance()} money.-----------\n");
        }
        public ATM(Bank bank, Money money)
        {
            Console.WriteLine("Create Class ATM\n");
            this.bank = bank;
            this.money = money;
        }
    }
}
