```plantuml
@startuml
namespace ATM {

class Bank {
-List<(int, long, string)> users
+List<(int, long, string)> Users()
+bool Conf(Card card)
+Bank(List<(int, long, string)> users)
}

class Money {
+int denomination
+Money(int denomination)
}

class Card {
-int balance
+int CVV
+long _number
+string _ownerName
+int GetBalance()
+int SetBalance(int value)
+Card(int CVV, long _number, string _ownerName, int balance)
}

class ATM {
-Card card
-Bank bank
-Money money
+void GetCard(Card card)
+void GiveCard()
+bool Auth()
+void GiveMoney(int amount)
+void GetMoney(int amount)
+void CheckMoney()
+ATM(Bank bank, Money money)
}

ATM -> Card
ATM -> Bank
ATM -down-> Money
Bank -up-> Card
}
@enduml
```
